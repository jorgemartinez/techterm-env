package main

import "github.com/nsf/termbox-go"

var BGColor = termbox.ColorBlue
var FGColor = termbox.ColorWhite

var width int
var height int

func main() {
	defer termbox.Close()

	termbox.Init()

	width, height = termbox.Size()

	var input = make(chan termbox.Event)
	var msgInput = make(chan string)

	go inputManager(input)
	go timeHandler(msgInput)
	programManager(input, msgInput)
}
