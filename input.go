package main

import "github.com/nsf/termbox-go"

func inputManager(input chan<- termbox.Event) {
	for {
		input <- termbox.PollEvent()
		if exit {
			break
		}
	}
}
