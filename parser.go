package main

import (
	"github.com/nsf/termbox-go"
	// "fmt"
	"strings"
)

func parseInput(msgInput chan<- string) {
	parsing = true
	commandlist := strings.Split(strings.TrimSpace(CurrentInput), ";")

forloop:
	for _, c := range commandlist {
		switch strings.TrimSpace(c) {
		case "clear":
			clear()
		case "":
		case "about":
			about()
		case "version":
			version()
		case "commands":
			commands()
		case "time":
			getTime()
		case "date":
			getDate()
		case "exit":
			exitTerm()
		default:
			commandNotExist(c)
			break forloop
		}
	}
	CurrentInput = ""

	termbox.SetCursor(CurrentX, CurrentY)
	termbox.Flush()

	parsing = false
	msgInput <- "progman:currentProgram-done"
}
