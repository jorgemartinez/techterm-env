package main

import (
	"fmt"
	"strings"

	"github.com/nsf/termbox-go"
)

/// Current Position Y
var CurrentY int

/// Current Position X
var CurrentX = 3

/// Input from user
var CurrentInput string
var parsing = false
var exit = false
var prompt = true

var something [256]string
var InputHistory []string

func programManager(input <-chan termbox.Event, msgInput chan string) {
	something[0] = "about"
	InputHistory = something[0:0]

	termbox.Clear(FGColor, BGColor)
	CurrentY = drawMenu()
	about()
	ChangeCurrent(3, 5)
	termbox.SetCursor(CurrentX, CurrentY)
	termbox.Flush()
	for {
		if exit {
			break
		}
		if !parsing && prompt {
			write("\\g->", 0, CurrentY, termbox.ColorGreen, BGColor)
			ChangeCurrent(3, CurrentY)
			termbox.SetCursor(CurrentX, CurrentY)
			termbox.Flush()
		}
		done := false
		select {
		case in := <-input:
			prompt = false
			done = CLInput(in)
			termbox.SetCursor(CurrentX, CurrentY)
		case mIn := <-msgInput:
			prompt = false
			MInput(mIn)
		default:
			prompt = false
			newwidth, newheight := termbox.Size()
			if newwidth != width {
				width = newwidth
				termbox.SetCursor(CurrentX, CurrentY)
				drawMenu()
			}
			if newheight != height {
				height = newheight
				termbox.SetCursor(CurrentX, CurrentY)
			}
		}
		if done { // Input is entered
			termbox.HideCursor()
			go parseInput(msgInput)
		}
	}
}

func CLInput(e termbox.Event) bool {
	if !parsing { // Keys to hand to the command line interface
		switch e.Key {
		case termbox.KeyDelete: // TODO
			break
		case termbox.KeyArrowLeft: // TODO
			break
		case termbox.KeyArrowRight: // TODO
			break
		case termbox.KeyArrowUp: // TODO
		case termbox.KeyArrowDown: // TODO
		case termbox.KeyBackspace:
			if CurrentX > 3 {
				CurrentX--
				write(" ", CurrentX, CurrentY, FGColor, BGColor)
				CurrentInput = CurrentInput[0 : len(CurrentInput)-1] // TODO: Make able to delete in middle of text
			}
		case termbox.KeyBackspace2:
			if CurrentX > 3 {
				CurrentX--
				write(" ", CurrentX, CurrentY, FGColor, BGColor)
				CurrentInput = CurrentInput[0 : len(CurrentInput)-1]
			}
		case termbox.KeyEnter:
			ChangeCurrent(0, CurrentY+1)
			//InputHistory = InputHistory[0:len(InputHistory) + 1]
			//InputHistory[len(InputHistory)] = CurrentInput
			return true
		default:
			CurrentX, CurrentY = write(string(e.Ch), CurrentX, CurrentY, FGColor, BGColor)
			CurrentInput = strings.TrimSpace(fmt.Sprint(CurrentInput, string(e.Ch)))
		}
		termbox.SetCursor(CurrentX, CurrentY)
		termbox.Flush()
		return false
	}
	// Keys to hand to current program
	switch e.Key {
	case termbox.KeyCtrlZ:
		// TODO: Stop current program!
		break
	}
	return false
}

func MInput(mIn string) {
	if strings.HasPrefix(mIn, "progman:") {
		switch mIn {
		case "progman:currentProgram-done":
			parsing = false
			prompt = true
		case "progman:updateTime":
			write(CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorGreen)
			termbox.Flush()
		}
	}
}

func ChangeCurrent(x, y int) {
	CurrentX = x
	CurrentY = y
}
