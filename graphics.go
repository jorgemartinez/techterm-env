package main

import "github.com/nsf/termbox-go"

func drawMenu() int {
	drawLine(0, 0, width, 0, termbox.ColorGreen)
	write("Apeeling-Tech TechTerm-Env - v0.2 alpha", 0, 0, termbox.ColorBlack, termbox.ColorGreen)
	write(CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorGreen)
	return 1
}

func write(s string, startX, startY int, fgcolor, bgcolor termbox.Attribute) (int, int) {
	currentx := startX
	currenty := startY
	doColor := fgcolor
	isSpecialChar := false
	for _, runeValue := range s {
		if runeValue == '\n' {
			currenty++
			currentx = 0
			continue
		} else if runeValue == '\t' {
			write("     ", currentx, currenty, fgcolor, bgcolor)
			currentx += 5
			continue
		} else if runeValue == '\\' {
			isSpecialChar = true
			continue
		}
		if isSpecialChar {
			if runeValue == 'g' {
				doColor = termbox.ColorGreen
			} else if runeValue == 'r' {
				doColor = termbox.ColorRed
			} else if runeValue == 'w' {
				doColor = termbox.ColorWhite
			} else if runeValue == 'd' {
				doColor = fgcolor
			}
			isSpecialChar = false
			continue
		}
		termbox.SetCell(currentx, currenty, runeValue, doColor, bgcolor)
		currentx++
	}
	termbox.Flush()
	return currentx, currenty
}

func drawLine(startX, startY, endX, endY int, bgcolor termbox.Attribute) (int, int) { // TODO: Improve to support diagonals
	for x := startX; x < endX; x++ {
		for y := startY; y <= endY; y++ {
			termbox.SetCell(x, y, ' ', FGColor, bgcolor)
		}
	}
	termbox.Flush()
	return 0, endY + 1
}
