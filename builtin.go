package main

import (
	"fmt"
	"time"

	"github.com/nsf/termbox-go"
)

func clear() {
	termbox.Clear(FGColor, BGColor)
	CurrentY = drawMenu()
	ChangeCurrent(3, 1)
}

func about() {
	_, CurrentY = write("Apeeling-Tech TechTerm-Env\nv0.2 alpha\nCopyright 2015. All Rights Reserved.", 0, CurrentY, FGColor, BGColor)
	CurrentY += 2
}

func version() {
	_, CurrentY = write("version 0.2 alpha", 0, CurrentY, FGColor, BGColor)
	CurrentY += 2
}

func commands() {
	_, CurrentY = write("clear     - Clears the screen\nabout     - Displays copyright information of the shell\nversion   - Displays version of the shell\nexit      - Exits the shell\ncommands  - Displays all available commands\ntime      - Shows the time\ndate      - Shows the date", 0, CurrentY, FGColor, BGColor)
	CurrentY += 2
}

func commandNotExist(c string) {
	_, CurrentY = write(fmt.Sprintf("The Command \"%s\" does not exist!", c), 0, CurrentY, FGColor, BGColor)
	CurrentY += 2
}

func getTime() {
	t := time.Now()
	_, CurrentY = write(fmt.Sprintf("%d:%d:%d %d/%d/%d", t.Hour(), t.Minute(), t.Second(), t.Month(), t.Day(), t.Year()), 0, CurrentY, FGColor, BGColor)
	CurrentY += 2
}

func getDate() {
	getTime()
}

func exitTerm() {
	clear()
	exit = true
}
